package WebpageInteraction;

import NeuralNetwork.NeuralNetworkControl;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.*;

public class WebInteractionControl implements Runnable{
    private String _webFileLoc = null;
    private WebPageURL _webPageURL = null;

    private boolean _running = true;

    private double[] _BTCCost = new double[1];
    private double _USDWallet = 0;
    private double _BTCWallet = 0;


    public WebInteractionControl(String baseFileLoc){
        _webFileLoc = baseFileLoc + "\\WebPage";
        String[] data = readKeys();
        _webPageURL = new WebPageURL(data[1], data[2], data[0]);
    }

    @Override
    public void run() {
        while(_running){
            //System.out.println("Updating Training Values");
            assignWalletValues(getWalletValues());
            //17100
            _BTCCost = getLastData("BTC");
            //NeuralNetworkControl.currentBitcoinValues = getCurrentTrainingData(4000, "BTC");
            delay(900000);
        }
    }

    public void placeOrder(String type, double amount, double currentCost){
        String bodyData = _webPageURL.emptyRequestAuthentication();
        String properAmount = String.format("\"amount\": %f", amount);
        bodyData += ",\n  \"type\": \"" + type + "\",\n  " + properAmount + ",\n  \"price\": ";
        //System.out.println(bodyData);
        if(type == "buy"){
            bodyData += "" + (currentCost + 50) + "\n}";
            //System.out.println(bodyData);
            System.out.println(_webPageURL.stringGetPage(true,"https://cex.io/api/place_order/BTC/USD", bodyData));
        }
        else if(type == "sell"){
            bodyData += "" + (currentCost - 50) + "\n}";
            System.out.println(_webPageURL.stringGetPage(true,"https://cex.io/api/place_order/BTC/USD", bodyData));
        }
        else{

        }
    }

    public double getUSDWallet(){
        return _USDWallet;
    }

    public double getBTCWallet(){
        return _BTCWallet;
    }

    public double getCurrentCost(){
        return _BTCCost[0];
    }

    public double[] getCostChartHistory(int numberOfHours, int numberOfChecks){
        double[] retVal = new double[numberOfChecks];
        String body = "{\n" +
                "  \"lastHours\": " + numberOfHours + ",\n" +
                "  \"maxRespArrSize\": "+ numberOfChecks + "\n" +
                "}";
        String data = _webPageURL.stringGetPage(true,"https://cex.io/api/price_stats/BTC/USD", body);
        if(!data.contains("error")) {
            String[] values = data.split("},");
            for (int i = 0; i < values.length; i++) {
                retVal[i] = Double.parseDouble(values[i].split(":")[2].replace("\"", "").replace("}]", ""));
            }
        }
        return retVal;
    }

    public double[] getCurrentTrainingData(int numOfIncrements, String Coin){
        if(NeuralNetworkControl.currentBitcoinValues == null ||
                NeuralNetworkControl.currentBitcoinValues[NeuralNetworkControl.currentBitcoinValues.length -1] == 0){
            NeuralNetworkControl.currentBitcoinValues = getNewTrainingData(numOfIncrements, Coin);
        }
        else{
            for(int i = 1; i < NeuralNetworkControl.currentBitcoinValues.length; i++){
                NeuralNetworkControl.currentBitcoinValues[i-1] = NeuralNetworkControl.currentBitcoinValues[i];
            }
            NeuralNetworkControl.currentBitcoinValues[NeuralNetworkControl.currentBitcoinValues.length-1] = getLastData(Coin)[0];
        }
        return NeuralNetworkControl.currentBitcoinValues;
    }

    public double[] getFirstValues(int numbValues){
        double[] retVal = null;
        if(NeuralNetworkControl.currentBitcoinValues != null) {
            retVal = getSubArray(NeuralNetworkControl.currentBitcoinValues,
                    NeuralNetworkControl.currentBitcoinValues.length - numbValues,
                    NeuralNetworkControl.currentBitcoinValues.length);
        }
        return retVal;
    }

    private double[] getSubArray(double[] mainArray, int starting, int ending){
        double[] retVal = new double[ending - starting];
        for(int i = starting; i < ending; i++){
            retVal[i - starting] = mainArray[i];
        }
        return retVal;
    }

    private double[] getNewTrainingData(int numOfIncrements, String Coin){
        double[] retVal = new double[numOfIncrements];
        int currentRetValLoc = 1;

        retVal[0] = getNewestCoinValue(Coin);

        double[] currentValues = getLastData(Coin);
        for(int i = 0; i < currentValues.length; i++){
            retVal[i + currentRetValLoc] = currentValues[i];
        }
        currentRetValLoc = currentValues.length;

        int currentDaysInPast = 1;
        while(currentRetValLoc < numOfIncrements){
            double[] previousValues = trainingDataForDay(getDate(currentDaysInPast), Coin);
            if(previousValues != null) {
                for (int i = 0; i < previousValues.length; i++) {
                    if(i + currentRetValLoc < retVal.length) {
                        retVal[i + currentRetValLoc] = previousValues[(previousValues.length - 1) - i];
                    }
                }
                currentRetValLoc += previousValues.length;
            }
            currentDaysInPast++;
        }

        return hotSwap(retVal);
    }

    private String[] readKeys(){
        String data = "";
        try {
            Scanner reader = new Scanner(new File(_webFileLoc + "//Keys.txt"));
            while(reader.hasNextLine()){
                data += reader.nextLine() + "=";
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String[] dataSplit = data.split("=");
        for(int i = 0; i < dataSplit.length; i++){
            dataSplit[i] = dataSplit[i].split(":")[1];
        }

        return dataSplit;
    }

    private void assignWalletValues(String[] values){
        if(values[0] != null && values[1] != null) {
            _BTCWallet = Double.parseDouble(values[0]);
            _USDWallet = Double.parseDouble(values[1]);
        }
    }

    private String[] getWalletValues(){
        String data = _webPageURL.stringGetPage(true,"https://cex.io/api/balance/", _webPageURL.emptyRequestAuthentication()+"\n}");
        String[] initalSeperation = data.split(",");
        String[] mergedData = new String[initalSeperation.length/2];
        String[] retVal = new String[2];
        int count = 0;
        for(int i = 0; i < mergedData.length; i++){
            mergedData[i] = initalSeperation[(i*2)] + "@" + initalSeperation[(i*2)+1];
        }
        for(int i = 0; i < mergedData.length; i++){
            if(mergedData[i].contains("\"BTC\"") || mergedData[i].contains("\"USD\"")){
                String firstVal = mergedData[i].split("@")[0];
                String[] datatest = firstVal.split("\":\"");
                retVal[count] = datatest[1].replace("\"", "");
                count++;
            }

        }
        return retVal;
    }

    private double[] hotSwap(double[] val){
        double[] retVal = new double[val.length];
        for(int i = 0; i < val.length; i++){
            retVal[i] = val[val.length - (1 + i)];
        }
        return retVal;
    }

    private double[] getLastData(String Coin){
        ArrayList<String> retVal = new ArrayList<>();
        boolean endFound = false;
        Point timeData = getTimeCommon(System.currentTimeMillis());
        String body = "{\n" +
                "  \"lastHours\": " + 24 + ",\n" +
                "  \"maxRespArrSize\": "+ 120 + "\n" +
                "}";
        String data = _webPageURL.stringGetPage(true,"https://cex.io/api/price_stats/" + Coin + "/USD", body);
        if(!data.contains("error")) {
            String[] values = data.replace("\"", "").split("},");
            for (int i = values.length - 1; i >= 0; i--) {
                if(!endFound){
                    String timeStamp = values[i].replace("[", "").replace("{", "").
                            split(",")[0].split(":")[1];
                    String value = values[i].replace("]", "").replace("}", "").
                            split(",")[1].split(":")[1];
                    Point loggedTime = getTimeCommon(Long.parseLong(timeStamp));
                    if(loggedTime.x == 0 && loggedTime.y == 0){
                        if(trainingDataForDay(getDate(1), Coin) != null){
                            endFound = true;
                        }
                        retVal.add(value);
                    }
                    else{
                        retVal.add(value);
                    }
                }
            }
        }
        return convertToArray(retVal);
    }

    private double[] convertToArray(ArrayList<String> stringArray){
        double[] retVal = null;
        if(stringArray != null) {
            retVal = new double[stringArray.size()];
            for (int i = 0; i < stringArray.size(); i++) {
                retVal[i] = Double.parseDouble(stringArray.get(i));
            }
        }
        return retVal;
    }

    private double[] trainingDataForDay(String time, String Coin){
        double[] retVal = null;
        String webLink = "https://cex.io/api/ohlcv/hd/" + time + "/" + Coin +"/USD";
        String data = _webPageURL.stringGetPage(true, webLink, "");
        if(!data.contains("null") && !data.contains("[]")){
            data = data.replace("\"", "");
            String[] dataTimes = data.split(":")[2].split("],");
            retVal = new double[96];
            int retValLoc = 0;
            Point targetTime = new Point(0,0);
            for(int i = 0; i < dataTimes.length; i++){
                dataTimes[i] = dataTimes[i].replace("[", "");
                String[] coreData = dataTimes[i].split(",");

                if(coreData.length > 1){
                    Point currentTime = getTimeCommon(Long.parseLong(coreData[0]));
                    if(currentTime.x >= targetTime.x && currentTime.y >= targetTime.y){
                        retVal[retValLoc] = Double.parseDouble(coreData[1]);
                        retValLoc++;
                        targetTime.y += 15;
                        if(targetTime.y >= 60){
                            targetTime.x++;
                            targetTime.y = targetTime.y - 60;
                        }
                    }
                }
            }
        }
        return retVal;
    }

    private double getNewestCoinValue(String Coin){
        String data = _webPageURL.stringGetPage(true,"https://cex.io/api/last_price/" + Coin +"/USD", "");
        data = data.replace("\"", "");
        data = data.split(",")[0];
        data = data.split(":")[1];
        return Double.parseDouble(data);
    }

    private Point getTimeCommon(long timeStamp){
        Date date = new java.util.Date(timeStamp*1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
        String[] time = sdf.format(date).split(":");
        Point retVal = new Point(Integer.parseInt(time[0]), Integer.parseInt(time[1]));

        return retVal;
    }

    private String getDate(int daysPrevious){
        long unixSeconds = System.currentTimeMillis();
        Date date = new java.util.Date(unixSeconds);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
        String formattedDate = sdf.format(date);
        String[] timeVals = formattedDate.split("-");
        int year = Integer.parseInt(timeVals[0]);
        int month = Integer.parseInt(timeVals[1]);
        int day = Integer.parseInt(timeVals[2]);
        for(int i = 0; i < daysPrevious; i++) {
            day--;
            if (day == 0) {
                month -= 1;
                if (month == 0) {
                    year -= 1;
                    month = 12;
                }
                Calendar c = Calendar.getInstance();
                c.set(year, month, 1);
                day = c.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
        }
        String sYear = year + "";
        String sMonth = month + "";
        String sDay = day + "";
        if(month < 10){
            sMonth = "0" + sMonth;
        }
        if(day < 10){
            sDay = "0" + sDay;
        }
        return "" + sYear + sMonth + sDay;
    }

    private void delay(int millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
