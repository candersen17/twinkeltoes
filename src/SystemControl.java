import GUI.GUI_Control;
import NeuralNetwork.BaseNeuralNetwork;
import NeuralNetwork.NeuralNetworkControl;
import WebpageInteraction.WebInteractionControl;

import java.awt.image.BufferedImage;

public class SystemControl {
    private static final long DATA_GRAB_DELAY_TIME = 900000;


    private static GUI_Control _guiControl = null;
    private static WebInteractionControl _webInteractionControl = null;
    private static NeuralNetworkControl _neuralNetworkControl = null;

    private static Thread _guiControlThread = null;
    private static Thread _webPageInteractionThread = null;

    private static long _dataGrabTimeStamp = 0;

    public static void main(String[] args){
        System.out.println("TwinkleToes Starting Up");

        System.out.println("Starting JFrame");
        initGUI();
        waitForMainScreen();
        initNeuralNetwork();
        initWebPageInteraction();
        delay(60000);

        while(true){
            delay(1000);
            _guiControl.setWalletAndCostValues(_webInteractionControl.getUSDWallet(),
                                                _webInteractionControl.getBTCWallet(),
                                                _webInteractionControl.getCurrentCost());
            _neuralNetworkControl.setTraining(_guiControl.getTrainingState());
            _neuralNetworkControl.updateCoreNetwork();

            if(_dataGrabTimeStamp < System.currentTimeMillis()){
                _dataGrabTimeStamp = System.currentTimeMillis() + DATA_GRAB_DELAY_TIME;
                NeuralNetworkControl.currentBitcoinValues = _webInteractionControl.getCurrentTrainingData(4000, "BTC");
                double[] vals = _webInteractionControl.getFirstValues(90);
                if(vals != null && _guiControl.getTradingState()) {
                    int networkResults = _neuralNetworkControl.getNetworkDecision(vals);
                    System.out.print(networkResults);
                    if(networkResults == 0){
                        _webInteractionControl.placeOrder("buy", .0004, _webInteractionControl.getCurrentCost());
                    }
                    else if(networkResults == 1){
                        _webInteractionControl.placeOrder("sell", .0004, _webInteractionControl.getCurrentCost());
                    }
                }
            }
        }
    }

    public static void delay(int millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void waitForMainScreen(){
        boolean state = _guiControl.getScreenPassedState();
        while(!state){
            state =  _guiControl.getScreenPassedState();
            delay(500);
            if(state){
                System.out.println("MovingForward");
            }
        }

    }

    private static void initNeuralNetwork(){
        _neuralNetworkControl = new NeuralNetworkControl(_guiControl.getDefaultFileLoc(), 90, 3, 10);
        //_neuralNetworkControl.trainNetwork(50, _webInteractionControl.getCurrentTrainingData(5000, "BTC"));
    }

    private static void initWebPageInteraction(){
        _webInteractionControl = new WebInteractionControl(_guiControl.getDefaultFileLoc());
        _webPageInteractionThread = new Thread(_webInteractionControl);
        _webPageInteractionThread.start();
    }

    private static void initGUI(){
        _guiControl = new GUI_Control();
        _guiControlThread = new Thread(_guiControl);
        _guiControlThread.start();
    }

}
