package GUI;



import com.sun.tools.javac.Main;

import javax.swing.*;

public class GUI_Control implements Runnable{
    private final int HEIGHT = 360;
    private String _defaultFileLoc = null;

    private int _screenState = 0;
    private JFrame _frame = null;

    private InitScreen _initScreen = null;
    private MainScreen _mainScreen = null;

    private boolean _screenPassedStartPage = false;

    public GUI_Control(){
        _frame = new JFrame();
        _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        _frame.setSize(1000, HEIGHT);
        _frame.setVisible(true);
    }

    @Override
    public void run() {
        while(true){
            stateMachine();
        }
    }

    public String getDefaultFileLoc(){
        return _defaultFileLoc;
    }

    public boolean getScreenPassedState(){
        return _screenPassedStartPage;
    }

    public boolean getTrainingState(){
        boolean retVal = false;
        if(_mainScreen != null){
            retVal = _mainScreen.getTrainingState();
        }
        return retVal;
    }

    public boolean getTradingState(){
        boolean retVal = false;
        if(_mainScreen != null){
            retVal = _mainScreen.getTradingState();
        }
        return retVal;
    }

    public void setWalletAndCostValues(double usdWallet, double btcWallet, double btcCost){
        _mainScreen.setValues(usdWallet, btcWallet, btcCost);
    }

    private void stateMachine(){
        switch(_screenState){
            case ADD_FILE_ASSIGNMENT_SCREEN:
                System.out.println("Adding Startup Screen");
                addStartUpScreen();
                _screenState = FILE_ASSIGNMENT_SCREEN_WAIT;
                break;
            case FILE_ASSIGNMENT_SCREEN_WAIT:
                fileAssignmentScreen();
                break;
            case MAIN_SCREEN:
                mainScreenLoop();
                break;
            default:
                _screenState = 0;
                break;
        }
        delay(100);
    }

    private void mainScreenLoop(){
        _mainScreen.screenUpdate();
        _frame.setSize(1000, HEIGHT);
        delay(100);
    }

    private void addMainScreen(){
        _mainScreen = new MainScreen();
        _frame.remove(_initScreen);
        _initScreen = null;
        _frame.repaint();
        _frame.add(_mainScreen);
    }

    private void addStartUpScreen(){
        _initScreen = new InitScreen();
        _frame.add(_initScreen);
        _initScreen.updateScreen();
    }

    private void fileAssignmentScreen(){
        _initScreen.updateScreen();
        if(_initScreen.getButtonClicked()){
            _defaultFileLoc = _initScreen.getFileLoc();
            addMainScreen();
            _screenState = MAIN_SCREEN;
            _screenPassedStartPage = true;
            System.out.println("Moving to Main Screen");
        }
        delay(100);
    }



    private void delay(int millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private static final int ADD_FILE_ASSIGNMENT_SCREEN = 0;
    private static final int FILE_ASSIGNMENT_SCREEN_WAIT = 1;
    private static final int MAIN_SCREEN = 2;
}
