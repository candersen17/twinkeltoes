package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InitScreen extends JPanel{
    private String _fileLoc = "C:\\Users\\curti\\OneDrive\\Projects\\TwinkleToes";
    private boolean _screenSet = false;
    private JTextField _textField = null;
    private JButton _continueButton;

    public InitScreen(){
        super();
        _continueButton = new JButton();
        _textField = new JTextField();
        this.add(_continueButton);
        this.add(_textField);
    }

    public void updateScreen(){
        this.setBackground(Color.gray);
        addButtonListener();
        addTextField();
    }

    public String getFileLoc(){
        return _fileLoc;
    }

    public boolean getButtonClicked(){
        return _screenSet;
    }

    private void addTextField(){
        _textField.setText(_fileLoc);
        _textField.setLocation(100,100);
    }

    private void addButtonListener(){
        _continueButton.setLocation(20, 20);
        _continueButton.setSize(400, 40);
        _continueButton.setBackground(Color.CYAN);
        _continueButton.setText("Continue");
        _continueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("ButtonClicked");
                _fileLoc = _textField.getText();
                _screenSet = true;
            }
        });
    }

}
