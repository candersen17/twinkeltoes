package GUI;

import NeuralNetwork.NetworkTrainer;
import NeuralNetwork.NeuralNetworkControl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainScreen extends JPanel {

    private double _USDWallet = 0;
    private double _BTCWallet = 0;
    private double _BTCCost = 0;

    private double _graphHigh = 0;
    private double _graphLow = Double.MAX_VALUE;

    private boolean _enableTraining = false;
    private boolean _enableTrading = false;

    private JButton _trainingEnableButton = null;
    private JButton _enableTradeButton = null;

    private JLabel _disValues = null;


    public MainScreen(){
        super();
        this.setBackground(Color.blue);
        this.setVisible(true);
        _disValues = new JLabel();
        this.add(_disValues);
        addTrainingButton();
        addTradingButton();
        this.add(_enableTradeButton);
        this.add(_trainingEnableButton);
    }

    public void screenUpdate(){
        this.setBackground(Color.lightGray);
        this.setSize(1000, 1000);
        displayGraphWithCosts(10, 110, 990, 200, NeuralNetworkControl.currentBitcoinValues);
        setValuesDis();
        updateTrainingButton();
        updateTradingButton();
    }

    public void setValues(double USDWallet, double BTCWallet, double BTCCost){
        _USDWallet = USDWallet;
        _BTCWallet = BTCWallet;
        _BTCCost = BTCCost;
    }

    public boolean getTrainingState(){
        return _enableTraining;
    }

    public boolean getTradingState(){
        return _enableTrading;
    }

    private void setValuesDis(){
        _disValues.setText("<html>USD Wallet = " + _USDWallet +
                            "<br>BTC Wallet = " + _BTCWallet +
                            "<br>BTC Cost = " + _BTCCost);
        _disValues.setLocation(170,30);
    }

    private void displayGraphWithCosts(int x, int y, int width, int height, double[] values){
        displayCurrentCostGraph(x,y,width - 200, height, values);
        this.getGraphics().drawString(_graphHigh + "", width - 170, y + 15);
        this.getGraphics().drawString(_graphLow + "", width - 170, y + height - 5);
        this.getGraphics().drawString("Blue = Buy", width - 170, y + 45);
        this.getGraphics().drawString("Cyan = Sell", width - 170, y + 65);
    }

    private void displayCurrentCostGraph(int x, int y, int width, int height, double[] values){
        Graphics g = this.getGraphics();
        g.setColor(Color.GRAY);
        g.drawRect(x, y, width, height);
        if(values != null) {
            double heightScale = 0;
            double widthScale = (double) width / (double) values.length;
            double maxHeight = 0;
            double minHeight = Double.MAX_VALUE;
            for (int i = 0; i < values.length; i++) {
                maxHeight = Math.max(maxHeight, values[i]);
                minHeight = Math.min(minHeight, values[i]);
            }
            heightScale = (double) height / (maxHeight - minHeight);
            for (int i = 1; i < values.length; i++) {
                if(_graphLow > values[i]){
                    g.setColor(Color.lightGray);
                    g.fillRect(width + x, y, 170, height);
                    _graphLow = values[i];
                }
                if(_graphHigh < values[i]){
                    g.setColor(Color.lightGray);
                    g.fillRect(width + x, y, 170, height);
                    _graphHigh = values[i];
                }
                Point point1 = new Point((int) (i * widthScale), (int) (height - ((values[i] - minHeight) * heightScale)));
                Point point2 = new Point((int) ((i - 1) * widthScale), (int) (height - ((values[i - 1] - minHeight) * heightScale)));
                point1.x += x;
                point1.y += y;
                point2.x += x;
                point2.y += y;

                if(NetworkTrainer.mainResultingValues != null) {
                    if (NetworkTrainer.mainResultingValues[i] == NeuralNetworkControl.BUY_VALUE) {
                        g.setColor(Color.blue);
                        g.drawLine(point1.x, y+2, point1.x, y + height - 1);
                    }
                    if (NetworkTrainer.mainResultingValues[i] == NeuralNetworkControl.SELL_VALUE) {
                        g.setColor(Color.cyan);
                        g.drawLine(point1.x, y+2, point1.x, y + height - 1);
                    }
                }

                if(point1.y < point2.y){
                    g.setColor(Color.green);
                }
                else{
                    g.setColor(Color.red);
                }
                g.drawLine(point1.x, point1.y, point2.x, point2.y);
            }
        }
        g = null;
    }

    //Training Button
    private void updateTrainingButton(){
        _trainingEnableButton.setLocation(10, 20);
        _trainingEnableButton.setSize(150, 30);
        if(NeuralNetworkControl.currentBitcoinValues != null){
            if(_enableTraining){
                _trainingEnableButton.setBackground(Color.green);
            }
            else{
                _trainingEnableButton.setBackground(Color.red);
            }
        }
        else{
            _trainingEnableButton.setBackground(Color.gray);
        }
    }

    private void addTrainingButton(){
        _trainingEnableButton = new JButton();
        _trainingEnableButton.setText("Training");
        _trainingEnableButton.setSize(150, 30);
        _trainingEnableButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(NeuralNetworkControl.currentBitcoinValues != null) {
                    _enableTraining = !_enableTraining;
                }
            }
        });
    }

    //Trading Button
    private void updateTradingButton(){
        _enableTradeButton.setLocation(10,60);
        _enableTradeButton.setSize(150, 30);
        if(NeuralNetworkControl.currentBitcoinValues != null){
            if(_enableTrading){
                _enableTradeButton.setBackground(Color.green);
            }
            else{
                _enableTradeButton.setBackground(Color.red);
            }
        }
        else{
            _enableTradeButton.setBackground(Color.gray);
        }
    }

    private void addTradingButton(){
        _enableTradeButton = new JButton();
        _enableTradeButton.setText("Trading");
        _enableTradeButton.setSize(30, 50);
        _enableTradeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(NeuralNetworkControl.currentBitcoinValues != null) {
                    _enableTrading = !_enableTrading;
                }
            }
        });
    }
}
