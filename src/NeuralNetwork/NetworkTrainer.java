/**
 * @Author Pr0xy4ctual
 * This is a network training system
 * Some of the missions of this class is that it runs independently until ended
 * It will calculate the results of the arrays in test in sub threads to speed up
 * the process of calculation
 */
package NeuralNetwork;

public class NetworkTrainer implements Runnable {
    public static double[] mainResultingValues = null;

    private final double STARTING_USD = 40;
    private final double STARTING_BTC = 0;
    private final double LOSSES_DUE_TO_TRADE = .02;

    private BaseNeuralNetwork _coreNetwork = null;
    private TrainerNetworkElement[] _trainers = null;
    private Thread[] _calculationThreads = null;
    private double _coreNetworkEndingValue = 0;
    private boolean _runTraining = true;
    private boolean _activelyTraining = false;

    /**
     * This is the constructor for this class. A small note is that the max number
     * of trainers determines the maximum number of threads that can be used for
     * calculation
     * @param maxTrainers int: max amount of threads allowed to be run for the training
     *                    system
     * @param network BaseNeuralNetwork: this will either be the neural network that
     *                is read in from the file or it will be the newly created network
     */
    public NetworkTrainer(int maxTrainers, BaseNeuralNetwork network){
        _coreNetwork = network;
        _trainers = new TrainerNetworkElement[maxTrainers];
        _calculationThreads = new Thread[maxTrainers];

        //calculation needed to get the initial value of the core network
        updateCoreNetworkTestValue();
    }

    /**
     * This is the system that is called when the thread is started.
     */
    @Override
    public void run() {
        _activelyTraining = true;
        _runTraining = true;
        System.out.println("Starting Training Threads");
        while (_runTraining){
            delay(100);
            TrainerNetworkElement newBest = getBestElement();
            if(newBest != null){
                _coreNetwork = newBest;
                _coreNetworkEndingValue = newBest.getWalletsWorth();
                System.out.println("new best outcome " + _coreNetworkEndingValue +
                        " sold : " + newBest.getTimesSold() + " bought : " + newBest.getTimesBought());
            }
        }
        System.out.println("Ending Training Threads");
        _activelyTraining = false;
    }

    /**
     * calling this tread will end all of the training while testing the last if the last
     * remaining open threads are completed
     */
    public void endTraining(){
        _runTraining = false;
    }

    /**
     * This gets if the Trainer is actively running
     * @return boolean: if the thread has completed
     */
    public boolean isActivelyTraining(){
        return _activelyTraining;
    }

    /**
     * this should be called roughly every 15 minutes with the update of the grabbed
     * values. It updates the best network value off of the new data.
     */
    public void updateCoreNetworkTestValue(){
        TrainerNetworkElement coreTrainingElement = new TrainerNetworkElement(_coreNetwork.toString(), STARTING_USD, STARTING_BTC, LOSSES_DUE_TO_TRADE);
        coreTrainingElement.run();
        mainResultingValues = coreTrainingElement.getResultingValues();
        _coreNetworkEndingValue = coreTrainingElement.getWalletsWorth();
        System.out.println("new System Core Val : "  + _coreNetworkEndingValue);
    }

    /**
     * this class should be called to get the current best network because this will grab
     * the currently most evolved network
     * @return BaseNeuralNetwork: This is what has been the best neural network
     */
    public BaseNeuralNetwork getCoreNetwork(){
        return _coreNetwork;
    }

    /*==============================Private Methods==============================*/

    private TrainerNetworkElement getBestElement(){
        TrainerNetworkElement retVal = null;
        for(int i = 0; i < _trainers.length; i++){
            if(_trainers[i] != null){
                //if the trained network ending worth is better than the current
                //network ending values.
                if(_trainers[i].getIsComplete() && _trainers[i].getWalletsWorth() > _coreNetworkEndingValue){
                    retVal = _trainers[i];
                }
                startNewTrainingThread(i);
            }
            else{
                startNewTrainingThread(i);
            }
        }
        //delay(75);
        return retVal;
    }

    private void startNewTrainingThread(int threadLoc){
        _calculationThreads[threadLoc] = null;
        _trainers[threadLoc] = null;
        delay(50);
        _trainers[threadLoc] = new TrainerNetworkElement(_coreNetwork.toString(), STARTING_USD, STARTING_BTC, LOSSES_DUE_TO_TRADE);
        _trainers[threadLoc].mutate(MutationNeuralNetwork.BASE_MUTATION_VALUES);
        _calculationThreads[threadLoc] = new Thread(_trainers[threadLoc]);
        _calculationThreads[threadLoc].start();
    }


    private void delay(int millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
