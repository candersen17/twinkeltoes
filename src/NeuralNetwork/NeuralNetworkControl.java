package NeuralNetwork;

import java.awt.*;
import java.io.*;
import java.util.Scanner;

public class NeuralNetworkControl{

    public static final int BUY_VALUE = 0;
    public static final int SELL_VALUE = 1;
    public static final int WAIT_VALUE = 2;

    public static double[] currentBitcoinValues = null;

    private NetworkTrainer _networkTrainer = null;
    private Thread _trainingThread = null;

    private String _fileLoc = "";

    private BaseNeuralNetwork _coreNetwork = null;

    private int _inputs = 0;
    private int _outputs = 0;
    private int _numbTrain = 0;

    public NeuralNetworkControl(String defaultFileLoc, int inputs, int outputs, int numberOfTrainers){
        _fileLoc = defaultFileLoc + "\\NeuralNetwork.txt";
        _inputs = inputs;
        _outputs = outputs;
        _coreNetwork = loadNetwork();
        _numbTrain = numberOfTrainers;
    }

    /**
     * this is to set the training system as on. If the system has not been initialized this will
     * properly start it. If the system is told to enable and it is already enabled it will
     * continue on as if noting happened. Important this will not stop the sub threads from completing
     * properly however it will stp more from replacing them.
     * @param trainingEnabled boolean: true to start up the neural net training system
     *                        false if to end the current training algorithm
     */
    public void setTraining(boolean trainingEnabled){
        if(trainingEnabled){
            if(_networkTrainer == null){
                _networkTrainer = new NetworkTrainer(500, _coreNetwork);
            }
            if(!_networkTrainer.isActivelyTraining()) {
                _trainingThread = new Thread(_networkTrainer);
                _trainingThread.start();
            }
        }
        else{
            if(_networkTrainer != null) {
                _networkTrainer.endTraining();
            }
        }
    }

    /**
     * updates the core network with the best one in the training neural network
     * one thing to note is that this will also overwrite the NeuralNetwork.txt file
     */
    public void updateCoreNetwork(){
        if(_networkTrainer != null){
            if(_coreNetwork != _networkTrainer.getCoreNetwork()){
                System.out.println("Updating Core network");
                _coreNetwork = _networkTrainer.getCoreNetwork();
                saveToFile(_networkTrainer.getCoreNetwork());
            }
        }
    }

    /**
     * It gets the decision of the core network something to make sure of is that
     * in using this if training network is running that the core network is updated
     * to have the best network at the time
     * @param values double[]: the last gathered values for a coin
     * @return int: this int will determine what is done farther up the line it is
     *          important to recognize that 0: is to buy 1: is for sell and 2: is to wait
     */
    public int getNetworkDecision(double[] values){
        double[] outputs = _coreNetwork.getOutputs(values);
        int bestVal = 0;
        for(int i = 1; i < outputs.length; i++){
            if(outputs[i] > outputs[bestVal]){
                bestVal = i;
            }
        }
        return bestVal;
    }

    private BaseNeuralNetwork loadNetwork(){
        String fileData = "";
        BaseNeuralNetwork retNet = null;
        try {
            Scanner scanner = new Scanner(new FileReader(new File(_fileLoc)));
            while(scanner.hasNextLine()){
                fileData += scanner.nextLine() + "\n";
            }
            scanner.close();
            retNet = new BaseNeuralNetwork(fileData);
            Point p = retNet.getNumInputsAndOutputs();
            if(p.x != _inputs || p.y != _outputs){
                System.out.println("Making new NeuralNetwork");
                retNet = makeOffInputsAndOutputs();
            }
        } catch (FileNotFoundException e) {
            retNet = makeOffInputsAndOutputs();
        }
        return retNet;
    }

    private BaseNeuralNetwork makeOffInputsAndOutputs(){
        BaseNeuralNetwork retNet = new BaseNeuralNetwork(_inputs, _outputs);
        saveToFile(retNet);
        return retNet;
    }

    private void saveToFile(BaseNeuralNetwork net){
        String[] lines = net.toString().split("\n");
        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(new File(_fileLoc)));
            for(int i = 0; i < lines.length; i++){
                br.write(lines[i]);
                br.newLine();
            }
            br.close();
        } catch (IOException e) {
        }

    }

    private void delay(int millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
