package NeuralNetwork;

import java.util.ArrayList;
import java.util.Random;

public class MutationNeuralNetwork extends BaseNeuralNetwork{
    public static final double[] BASE_MUTATION_VALUES = {
            .5,     //Edit Edge Weight
            .1,     //Add Edge
            .05,    //Add Node
            .3,     //Change Offset
            .05,    //Remove Edge
            .05,    //RemoveNode
            .01     //Fresh Start
    };

    public static final int RAND_AMOUNT = 1000000;

    private static final Random _rand = new Random();

    /**
     * This is the constructor nothing is different from the BaseNeuralNetwork
     * it just has to call the super
     * @param data String of all the data needed to make the network
     */
    public MutationNeuralNetwork(String data) {
        super(data);
    }

    /**
     * Mutates the current network that way that when it is tested it is a different
     * network being tested in the training program.
     * @param values a double array that is used to determine the chances that a
     *               particular element will be mutated
     */
    public void mutate(double[] values){
        mutateEditEdgeWeight(values[0]);
        mutateAddEdge(values[1]);
        mutateAddNode(values[2]);
        mutateChangOffset(values[3]);
        mutateRemoveEdge(values[4]);
        mutateRemoveNode(values[5]);
        mutateMakeNewRandom(values[6]);
        verifyNoFloatingNodes();
        //System.out.println(this.toString());
    }

    private void verifyNoFloatingNodes(){
        int[] ref = new int[this._netNodes.size()];
        for(int i = 0; i < ref.length; i++){
            NetNode[] edges = this._netNodes.get(i).getEdges();
            for(int e = 0; e < edges.length; e++){
                ref[this._netNodes.indexOf(edges[e])]++;
            }
        }
        for(int i = ref.length - 1; i >= 0; i--){
            if(ref[i] == 0 || this._netNodes.get(i).getEdges().length == 0){
                if(this._netNodes.get(i).getNodeType() != NetNode.TYPE_INPUT && this._netNodes.get(i).getNodeType() != NetNode.TYPE_OUTPUT){
                    removeNode(this._netNodes.get(i));
                }
            }
        }
    }

    private void mutateMakeNewRandom(double chance){
        if(chance(chance)){
            int inputs = this.getNodeType(NetNode.TYPE_INPUT).length;
            int outputs = this.getNodeType(NetNode.TYPE_OUTPUT).length;
            int hidden = this.getNodeType(NetNode.TYPE_HIDDEN).length;
            int initialNumberOfEdges = (inputs + outputs)/2;
            this._netNodes = new ArrayList<>();
            for(int i = 0; i < inputs; i++){
                addNode(_netNodes.size(), NetNode.TYPE_INPUT, 0, 0);
            }
            for(int i = 0; i < outputs; i++){
                addNode(_netNodes.size(), NetNode.TYPE_OUTPUT, MAX_LAYER, 0);
            }
            for(int i = 0; i < hidden; i++){
                addRandomNode();
            }
            for(int i = 0; i < initialNumberOfEdges; i++){
                addRandomEdge();
            }

        }
    }

    private void mutateRemoveNode(double chance){
        if(chance(chance)) {
            NetNode[] hiddenNodes = this.getNodeType(NetNode.TYPE_HIDDEN);
            if(hiddenNodes.length > 0){
                NetNode selectedNode = hiddenNodes[Math.abs(_rand.nextInt() % hiddenNodes.length)];
                removeNode(selectedNode);
            }
        }

    }

    private void mutateRemoveEdge(double chance){
        if(chance(chance)){
            NetNode[] validNodes = nodesNotOfType(NetNode.TYPE_INPUT);
            NetNode selectedNode = validNodes[Math.abs(_rand.nextInt() % validNodes.length)];
            if(selectedNode.getEdges().length > 0) {
                selectedNode.removeEdge(selectedNode.getEdges()[Math.abs(_rand.nextInt() % selectedNode.getEdges().length)]);
            }
        }
    }

    private void mutateEditEdgeWeight(double chance){
        for(int node = 0; node < this._netNodes.size(); node++){
            NetNode[] edgeNodes = _netNodes.get(node).getEdges();
            double[] edgeWeights = _netNodes.get(node).getEdgeWeights();
            for(int edge = 0; edge < edgeNodes.length; edge++){
                if(chance(chance)){
                    this._netNodes.get(node).addEdge(edgeNodes[edge], edgeWeights[edge] + (double)(_rand.nextInt() % RAND_AMOUNT/10000)/RAND_AMOUNT);
                }
            }
        }
    }

    private void mutateAddEdge(double chance){
        if(chance(chance)){
            NetNode node = this._netNodes.get(Math.abs(_rand.nextInt() % _netNodes.size()));
            if(node.getNodeType() != NetNode.TYPE_INPUT){
                NetNode[] nodes = this.getOpenEdges(node);
                NetNode end = nodes[Math.abs(_rand.nextInt() % nodes.length)/nodes.length];
                if(end != null){
                    node.addEdge(end, (double)(_rand.nextInt() % RAND_AMOUNT)/(RAND_AMOUNT*10));
                }
            }
        }
    }

    private void mutateAddNode(double chance){
        if(chance(chance)){
            addRandomNode();
        }
    }

    private void mutateChangOffset(double chance){
        for(int i = 0; i < this._netNodes.size(); i++){
            if(chance(chance)){
                this._netNodes.get(i).setOffset(this._netNodes.get(i).getOffset() + (double)(_rand.nextInt() % RAND_AMOUNT/1000)/RAND_AMOUNT);
            }
        }
    }

    private void addRandomNode(){
        NetNode startNode = nodesNotOfType(NetNode.TYPE_INPUT)[Math.abs(_rand.nextInt() % nodesNotOfType(NetNode.TYPE_INPUT).length)];
        NetNode endNode = this.nodesLessThan(startNode.getNodeLevel())[Math.abs(_rand.nextInt() % this.nodesLessThan(startNode.getNodeLevel()).length)];
        double newNodeLevel = ((startNode.getNodeLevel() - endNode.getNodeLevel()) / 2) + endNode.getNodeLevel();
        NetNode newNode = this.addNode(this._netNodes.size(), NetNode.TYPE_HIDDEN, newNodeLevel, 0);
        NetNode[] nodesAboveNew = nodesLessThan(newNode.getNodeLevel());
        this.addEdge(newNode, nodesAboveNew[Math.abs(_rand.nextInt()%nodesAboveNew.length)], (double)(_rand.nextInt() % RAND_AMOUNT)/RAND_AMOUNT);
        this.addEdge(newNode, nodesAboveNew[Math.abs(_rand.nextInt()%nodesAboveNew.length)], (double)(_rand.nextInt() % RAND_AMOUNT)/RAND_AMOUNT);
        this.addEdge(nodesAboveLevel(newNode.getNodeLevel())[Math.abs(_rand.nextInt()%nodesAboveLevel(newNode.getNodeLevel()).length)], newNode, (double)(_rand.nextInt() % RAND_AMOUNT)/RAND_AMOUNT);
    }

    private void removeNode(NetNode node){
        for(int i = 0; i < this._netNodes.size(); i++){
            this._netNodes.get(i).removeEdge(node);
        }
        this._netNodes.remove(this._netNodes.indexOf(node));
    }

    private NetNode[] nodesAboveLevel(double level){
        ArrayList<NetNode> retVal = new ArrayList<>();
        for(int i = 0; i < this._netNodes.size(); i++){
            if(this._netNodes.get(i).getNodeLevel() > level){
                retVal.add(this._netNodes.get(i));
            }
        }
        return retVal.toArray(new NetNode[retVal.size()]);
    }

    private NetNode[] nodesNotOfType(int nodeType){
        ArrayList<NetNode> retVal = new ArrayList<>();
        for(int i = 0 ; i < this._netNodes.size(); i++){
            if(this._netNodes.get(i).getNodeType() != nodeType){
                retVal.add(this._netNodes.get(i));
            }
        }
        return retVal.toArray(new NetNode[retVal.size()]);
    }

    private NetNode selectRandom(NetNode[] nodes){
        return nodes[Math.abs(_rand.nextInt()%nodes.length)];
    }

    private boolean chance(double chance){
        double value = (double) Math.abs(_rand.nextInt() % RAND_AMOUNT)/RAND_AMOUNT;
        return value < chance;
    }
}
