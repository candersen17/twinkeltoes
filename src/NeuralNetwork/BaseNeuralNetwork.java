package NeuralNetwork;


import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class BaseNeuralNetwork {
    public static final double MAX_LAYER = 1;
    public static final Random RAND = new Random();

    protected ArrayList<NetNode> _netNodes = new ArrayList<>();

    public BaseNeuralNetwork(int inputs, int outputs){
        int initialNumberOfEdges = (inputs + outputs) / 2;
        for(int i = 0; i < inputs; i++){
            addNode(_netNodes.size(), NetNode.TYPE_INPUT, 0, 0);
        }
        for(int i = 0; i < outputs; i++){
            addNode(_netNodes.size(), NetNode.TYPE_OUTPUT, MAX_LAYER, 0);
        }
        for(int i = 0; i < initialNumberOfEdges; i++){
            addRandomEdge();
        }
    }

    public BaseNeuralNetwork(String data){
        String[] nodes = data.split("\n");
        String[] nodeList = new String[nodes.length];
        String[] edgeList = new String[nodes.length];
        if(data != "") {
            for (int i = 0; i < nodes.length; i++) {
                String[] nodeData = nodes[i].split("#");
                nodeList[i] = nodeData[0];
                if (nodeData.length == 2) {
                    edgeList[i] = nodeData[1];
                } else {
                    edgeList[i] = "";
                }
            }

            addManyNodes(nodeList);
            for (int i = 0; i < nodeList.length; i++) {
                NetNode node = getNode(Integer.parseInt(nodeList[i].split(",")[0]));
                String[] edgeData = edgeList[i].split(":");
                addManyEdges(edgeData, node);
            }
        }
    }

    public double[] getOutputs(double[] inputs){
        NetNode[] inputNodes = getNodeType(NetNode.TYPE_INPUT);
        NetNode[] outputNodes = getNodeType(NetNode.TYPE_OUTPUT);
        double[] outputs = new double[outputNodes.length];
        if(inputs.length != inputNodes.length){
            return null;
        }
        for(int i = 0 ; i < inputNodes.length; i++){
            inputNodes[i].setValue(inputs[i]);
        }
        for(int i = 0; i < outputs.length; i++){
            outputs[i] = outputNodes[i].getNodeValue();
        }
        for(int i = 0; i < _netNodes.size(); i++){
            _netNodes.get(i).wipeValue();
        }
        return outputs;
    }

    public Point getNumInputsAndOutputs(){
        return new Point(getNodeType(NetNode.TYPE_INPUT).length, getNodeType(NetNode.TYPE_OUTPUT).length);
    }

    public String toString(){
        String retString = "";
        for(int i=0; i < _netNodes.size(); i++){
            retString += _netNodes.get(i).toString() + "\n";
        }
        return retString;
    }

    protected NetNode addNode(int ID, int type, double level, double offset){
        NetNode node = new NetNode(ID, type, level, offset, null, null);
        int count = 0;
        while(count < _netNodes.size() && level >= _netNodes.get(count).getNodeLevel()){
            count++;
        }
        _netNodes.add(count, node);
        return node;
    }

    protected void addEdge(NetNode holder, NetNode ending, double weight){
        holder.addEdge(ending, weight);
    }

    protected void addRandomEdge(){
        NetNode[] outputs = getNodeType(NetNode.TYPE_OUTPUT);
        NetNode randomSelectedNode = outputs[Math.abs(RAND.nextInt() % outputs.length)];
        NetNode[] usableEdgeConnections = getOpenEdges(randomSelectedNode);
        if(usableEdgeConnections.length >= 1){
            NetNode chosenEdge = usableEdgeConnections[Math.abs(RAND.nextInt() % usableEdgeConnections.length)];
            double weight = 0;
            addEdge(randomSelectedNode, chosenEdge, weight);
        }
    }

    protected NetNode[] nodesLessThan(double level){
        ArrayList<NetNode> retNodes = new ArrayList<>();
        for(int i = 0; i < _netNodes.size(); i ++){
            if(_netNodes.get(i).getNodeLevel() < level){
                retNodes.add(_netNodes.get(i));
            }
        }
        return retNodes.toArray(new NetNode[retNodes.size()]);
    }

    protected NetNode[] getOpenEdges(NetNode node){
        ArrayList<NetNode> retNodes = new ArrayList<>();
        NetNode[] nodesInQuestion = nodesLessThan(node.getNodeLevel());
        for(int i = 0; i < nodesInQuestion.length; i ++){
            if(!node.containsEdge(nodesInQuestion[i])){
                retNodes.add(nodesInQuestion[i]);
            }
        }
        return retNodes.toArray(new NetNode[retNodes.size()]);
    }

    protected NetNode[] getNodeType(int nodeType){
        ArrayList<NetNode> retNodes = new ArrayList<>();
        for(int i = 0; i < _netNodes.size(); i ++){
            if(_netNodes.get(i).getNodeType() == nodeType){
                retNodes.add(_netNodes.get(i));
            }
        }
        return retNodes.toArray(new NetNode[retNodes.size()]);
    }

    protected NetNode[] getAllNodes(){
        return _netNodes.toArray(new NetNode[_netNodes.size()]);
    }

    private NetNode getNode(int id){
        for(int i = 0; i < _netNodes.size(); i++){
            if(_netNodes.get(i).getNodeID() == id){
                return _netNodes.get(i);
            }
        }
        return null;
    }

    private void addManyNodes(String[] nodeTypes){
        if(nodeTypes[0] != "") {
            for (int i = 0; i < nodeTypes.length; i++) {
                String[] data = nodeTypes[i].split(",");
                addNode(Integer.parseInt(data[0]), Integer.parseInt(data[1]), Double.parseDouble(data[2]), Double.parseDouble(data[3]));
            }
        }
    }

    private void addManyEdges(String[] edgeTypes, NetNode node){
        if(edgeTypes[0] != "") {
            for (int i = 0; i < edgeTypes.length; i++) {
                String[] rawEdge = edgeTypes[i].split(",");
                addEdge(node, getNode(Integer.parseInt(rawEdge[0])), Double.parseDouble(rawEdge[1]));
            }
        }
    }
}

class NetNode{
    public static final int TYPE_INPUT = 0;
    public static final int TYPE_HIDDEN = 1;
    public static final int TYPE_OUTPUT = 2;

    private HashMap<NetNode, String> _edges = new HashMap<>();

    private boolean _valueSet = false;
    private int _nodeType = 0;
    private int _nodeID = 0;
    private double _nodeLevel = 0;
    private double _nodeOffset = 0;
    private double _value = 0;

    public NetNode(int ID, int type, double level, double offset, NetNode[] edges, double[] edgeWeights){
        _nodeID = ID;
        _nodeType = type;
        _nodeLevel = level;
        _nodeOffset = offset;
        if(edges != null && edgeWeights != null && edges.length == edgeWeights.length) {
            for (int i = 0; i < edges.length; i++) {
                addEdge(edges[i], edgeWeights[i]);
            }
        }
    }

    public void addEdge(NetNode node, double weight){
        if(node != null && node.getNodeLevel() < _nodeLevel){
            _edges.put(node, "" + weight);
        }
    }

    public void removeEdge(NetNode node){
        _edges.remove(node);
    }

    public void wipeValue(){
        _valueSet = false;
    }

    public void setOffset(double offset){
        _nodeOffset = offset;
    }

    public void setValue(double value){
        _value = value;
        _valueSet = true;
    }

    public int getNodeType(){
        return _nodeType;
    }

    public int getNodeID(){
        return _nodeID;
    }

    public double getOffset(){
        return _nodeOffset;
    }

    public double getNodeValue(){
        if(_valueSet){
            return _value;
        }
        double retVal = 0;
        NetNode[] keys = _edges.keySet().toArray(new NetNode[_edges.keySet().size()]);
        for (int i = 0; i < keys.length; i++) {
            retVal += keys[i].getNodeValue() * Double.parseDouble(_edges.get(keys[i]));
        }
        _value = retVal;
        _valueSet = true;
        return retVal;
    }

    public double getNodeLevel(){
        return _nodeLevel;
    }

    public boolean containsEdge(NetNode node){
        return _edges.containsKey(node);
    }

    public double[] getEdgeWeights(){
        NetNode[] edges = getEdges();
        double[] weights = new double[edges.length];
        for(int i = 0; i < edges.length; i++){
            weights[i] = Double.parseDouble(_edges.get(edges[i]));
        }
        return weights;
    }

    public NetNode[] getEdges(){
        return _edges.keySet().toArray(new NetNode[_edges.keySet().size()]);
    }

    public String toString(){
        String retString = getNodeID() + "," + getNodeType() + "," + getNodeLevel() + "," + _nodeOffset + "#";
        Set<NetNode> data = _edges.keySet();
        NetNode[] nodes = data.toArray(new NetNode[data.size()]);
        for(int i = 0; i < nodes.length; i++){
            String edgeLabels = nodes[i].getNodeID() + "," + _edges.get(nodes[i]);
            retString += edgeLabels + ":";
        }
        return retString;
    }
}
