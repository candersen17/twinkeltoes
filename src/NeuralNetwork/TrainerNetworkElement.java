package NeuralNetwork;

public class TrainerNetworkElement extends MutationNeuralNetwork implements Runnable{
    private double _usdWalletAmount = 0;
    private double _btcWalletAmount = 0;
    private double _lossesToTrade = 0;

    private double[] results = new double[NeuralNetworkControl.currentBitcoinValues.length];
    private int currentLoc = 0;

    private int _timesSold = 0;
    private int _timesBought = 0;

    private boolean _isComplete = false;



    public TrainerNetworkElement(String data, double startingUSD, double startingBTC, double lossesToTrade) {
        super(data);
        _lossesToTrade = lossesToTrade;
        _btcWalletAmount = startingBTC;
        _usdWalletAmount = startingUSD;
    }

    @Override
    public void run() {
        currentLoc = this.getNodeType(NetNode.TYPE_INPUT).length;
        int numberOfInputs = this.getNodeType(NetNode.TYPE_INPUT).length;
        for(int i = 0; i < numberOfInputs; i++){
            results[i] = NeuralNetworkControl.WAIT_VALUE;
        }
        for(int i = 1; i < NeuralNetworkControl.currentBitcoinValues.length - numberOfInputs; i++){
            double[] outputs = this.getOutputs(getSubArray(NeuralNetworkControl.currentBitcoinValues, i, i+numberOfInputs));
            actOnOutputs(outputs, NeuralNetworkControl.currentBitcoinValues[i + numberOfInputs]);
        }
        _isComplete = true;
    }

    public double getWalletsWorth(){
        return _usdWalletAmount + ((_btcWalletAmount/1.5) * NeuralNetworkControl.currentBitcoinValues[NeuralNetworkControl.currentBitcoinValues.length - 1]);
    }

    public boolean getIsComplete(){
        return _isComplete;
    }

    private double[] getSubArray(double[] mainArray, int starting, int ending){
        double[] retVal = new double[ending - starting];
        for(int i = starting; i < ending; i++){
            retVal[i - starting] = mainArray[i];
        }
        return retVal;
    }

    public double[] getResultingValues(){
        return results;
    }


    public int getTimesSold(){
        return _timesSold;
    }

    public int getTimesBought(){
        return _timesBought;
    }

    private void actOnOutputs(double[] outputs, double currentCost){
        int bestOutcome = 0;
        for(int i = 1; i < outputs.length; i++){
            if(outputs[i] > outputs[bestOutcome]){
                bestOutcome = i;
            }
        }
        //Selling the bitcoin
        if(bestOutcome == NeuralNetworkControl.SELL_VALUE){
            sellBitcoin(10, currentCost);
        }

        //Buying the bitcoin
        else if(bestOutcome == NeuralNetworkControl.BUY_VALUE){
            buyBitcoin(10, currentCost);
        }
        results[currentLoc++] = bestOutcome;


        //Other wise it does nothing
    }

    private void buyBitcoin(double amount, double bitcoinCost){
        double cost = amount + (amount * _lossesToTrade);
        bitcoinCost += 50;
        _timesBought++;
        if(_usdWalletAmount >= cost){
            _usdWalletAmount -= cost;
            _btcWalletAmount += amount / bitcoinCost;
        }

        //this is here so that hopefully the system will evolve in such a way as
        //to include buying and selling.
        else{
            _btcWalletAmount -= amount / (1000 * bitcoinCost);
        }
    }

    private void sellBitcoin(double amount, double bitcoinCost){
        bitcoinCost -= 50;
        double cost = amount / bitcoinCost;
        _timesSold++;
        if(cost <= _btcWalletAmount){
            _usdWalletAmount += amount - (amount * _lossesToTrade);
            _btcWalletAmount -= cost;
        }

        //this is to promote the evolution of the neural network so that it includes
        //buying and selling
        else{
            _usdWalletAmount -= amount / 1000;
        }
    }
}
